/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Findings_TestClasses;


import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Findings_PageObjects.Audit_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Findings Part 4",
        createNewBrowserInstance = false
)

public class FR1_Capture_Findings_MainScenario_Part4 extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR1_Capture_Findings_MainScenario_Part4()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!startAudit())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        
        
        return narrator.finalizeTest("Completed Capture Audit");
    }
    
    public boolean startAudit(){
        //Start Audit button
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.startAuditButton(), 10000)){
            error = "Failed to wait for 'Start Audit' button.";
            return false;
        }
        if(!SeleniumDriverInstance.scrollToElement(Audit_PageObjects.startAuditButton())){
            error = "Failed to scroll to 'Start Audit' button.";
            return false;
        }
        pause(5000);
        narrator.stepPassedWithScreenShot("Successfully scrolled to 'Start Audit' button.");
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.startAuditButton())){
            error = "Failed to wait for 'Start Audit' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.startAuditButton())){
            error = "Failed to click on 'Start Audit' button.";
            return false;
        }
        if (SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Audit_PageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        pause(10000);
        narrator.stepPassedWithScreenShot("Successfully clicked 'Start Audit' button.");
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Audit_PageObjects.auditRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
         
        
//        if(getData("Sign Off").equalsIgnoreCase("TRUE")){
//            SeleniumDriverInstance.Driver.close();
//        }

        return true;
    }

}
