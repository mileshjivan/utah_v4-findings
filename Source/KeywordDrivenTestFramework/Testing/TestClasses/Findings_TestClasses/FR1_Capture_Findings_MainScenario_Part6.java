/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Findings_TestClasses;


import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Findings_PageObjects.Audit_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Findings_PageObjects.Findings_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author RNagel
 */

@KeywordAnnotation(
        Keyword = "Capture Findings Part 6",
        createNewBrowserInstance = false
)

public class FR1_Capture_Findings_MainScenario_Part6 extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR1_Capture_Findings_MainScenario_Part6()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {    
        if(!enterDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Completed navigate to Audit");
    }
    
    //Enter data
    public boolean enterDetails(){  
        //Root Cause dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Findings_PageObjects.CFM_RootCause_dropdown())){
            error = "Failed to wait for Root cause dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Findings_PageObjects.CFM_RootCause_dropdown())){
            error = "Failed to click Root cause dropdown.";
            return false;
        }
        //Root Cause select
        if(!SeleniumDriverInstance.waitForElementByXpath(Findings_PageObjects.CFM_RootCause_select(testData.getData("Root Cause")))){
            error = "Failed to wait for '"+testData.getData("Root Cause")+"' in Root Cause dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Findings_PageObjects.CFM_RootCause_select(testData.getData("Root Cause")))){
            error = "Failed to click '"+testData.getData("Root Cause")+"' from Root Cause dropdown.";
            return false;
        }
        //Root Cause arrow
        if(!SeleniumDriverInstance.waitForElementByXpath(Findings_PageObjects.CFM_RootCause_arrow())){
            error = "Failed to wait for Root cause arrow.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Findings_PageObjects.CFM_RootCause_arrow())){
            error = "Failed to click Root cause arrow.";
            return false;
        }
        //Root Cause Comments textarea
        if(!SeleniumDriverInstance.waitForElementByXpath(Findings_PageObjects.CFM_RCC_textarea())){
            error = "Failed to wait for Root Cause Comments textarea.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Findings_PageObjects.CFM_RCC_textarea(), testData.getData("Root Cause Comments"))){
            error = "Failed to enter text into Root Cause Comments textarea.";
            return false;
        }
        
        //Save
        if(!SeleniumDriverInstance.waitForElementByXpath(Findings_PageObjects.Save())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Findings_PageObjects.Save())){
            error = "Failed to click 'Save' button.";
            return false;
        }
        
        pause(5000);
        //Save Mask
        if (SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Audit_PageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Audit_PageObjects.auditFindingsRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());  
        
//        //Check if the record has been Saved
//        if(!SeleniumDriverInstance.waitForElementByXpath(Findings_PageObjects.inspection_RecordSaved_popup())){
//            error = "Failed to wait for 'Record Saved' popup.";
//            return false;
//        }
//        
//        String saved = SeleniumDriverInstance.retrieveTextByXpath(Findings_PageObjects.inspection_RecordSaved_popup());
//        
//        if(saved.equals("Record saved")){
//            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
//        }else{   
//            if(!SeleniumDriverInstance.waitForElementByXpath(Findings_PageObjects.failed())){
//                error = "Failed to wait for error message.";
//                return false;
//            }
//
//            String failed = SeleniumDriverInstance.retrieveTextByXpath(Findings_PageObjects.failed());
//
//            if(failed.equals("ERROR: Record could not be saved")){
//                error = "Failed to save record.";
//                return false;
//            }
//        }
        return true;
    }

}
