/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Findings_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Findings_PageObjects.Audit_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Findings_PageObjects.Findings_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Capture Findings Part 3",
        createNewBrowserInstance = false
)

public class FR1_Capture_Findings_MainScenario_Part3 extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_Findings_MainScenario_Part3() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");

        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());

    }

    public TestResult executeTest() {
        if (!navigateToAudit()) {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!updateAuditManager()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Completed navigate to Audit and saved record :#" + getRecordId());
    }

    public boolean navigateToAudit(){
        
        pause(5000);
        //Navigate to Environmental Health & Safety
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.navigate_EHS())){
            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.navigate_EHS())){
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");
        
//        //Navigate to Audit & Inspections
//        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audits_Inspections())){
//            error = "Failed to wait for 'Audit & Inspections.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audits_Inspections())){
//            error = "Failed to click on 'Audit & Inspections' tab.";
//            return false;
//        }
//        SeleniumDriverInstance.pause(2000);
//        narrator.stepPassedWithScreenShot("Successfully navigated to 'Audit & Inspections' tab.");
//       
        
        //Navigate to Audit & Inspections
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.firstAudits())){
            error = "Failed to wait for 'Audit & Inspections.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.firstAudits())){
            error = "Failed to click on 'Audit & Inspections' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Audit & Inspections' tab.");
       
        
        //Navigate to Audits
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audits())){
            error = "Failed to wait for 'Audits' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audits())){
            error = "Failed to click on 'Audits' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(5000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Audits' tab.");
        
        //Search button
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.SearchButton())){
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.SearchButton())){
            error = "Failed to click on 'Search' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");
        
        //Selecting the created record
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.globalCompanyRecord(), 10000)){
            error = "Failed to wait for 'Global Company' record.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.globalCompanyRecord())){
            error = "Failed to click on 'Global Company' record.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click on 'Global Company' record.");

        return true;
    }
    
    public boolean updateAuditManager(){
        //process flow
        pause(10000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.processFlowManager(), 10000)){
            error = "Failed to wait for 'Process Flow' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.processFlowManager())){
            error = "Failed to click on 'Process Flow' button.";
            return false;
        }
        //Accept Proposed date
        if(!SeleniumDriverInstance.scrollToElement(Audit_PageObjects.acceptProposedDatesChckbx())){
            error = "Failed to scroll to scheckbox";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.acceptProposedDatesChckbx())){
            error = "Failed to wait for 'Proposed dates' check box.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.acceptProposedDatesChckbx())){
            error = "Failed to click on 'Proposed dates' check box.";
            return false;
        }
        //Save button
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.saveButton())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.saveButton())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        pause(10000);
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Audit_PageObjects.auditRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
         
//        
//        //Save Mask
//        if (SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.saveWait(), 2)) {
//            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Audit_PageObjects.saveWait2(), 400)) {
//                error = "Webside too long to load wait reached the time out";
//                return false;
//            }
//        }
//        
//        //Check if the record has been Saved
//        if(!SeleniumDriverInstance.waitForElementByXpath(Findings_PageObjects.inspection_RecordSaved_popup())){
//            error = "Failed to wait for 'Record Saved' popup.";
//            return false;
//        }
//        
//        String saved = SeleniumDriverInstance.retrieveTextByXpath(Findings_PageObjects.inspection_RecordSaved_popup());
//        
//        if(saved.equals("Record saved")){
//            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
//        }else{   
//            if(!SeleniumDriverInstance.waitForElementByXpath(Findings_PageObjects.failed())){
//                error = "Failed to wait for error message.";
//                return false;
//            }
//
//            String failed = SeleniumDriverInstance.retrieveTextByXpath(Findings_PageObjects.failed());
//
//            if(failed.equals("ERROR: Record could not be saved")){
//                error = "Failed to save record.";
//                return false;
//            }
//        }
        return true;
    }
    
}
