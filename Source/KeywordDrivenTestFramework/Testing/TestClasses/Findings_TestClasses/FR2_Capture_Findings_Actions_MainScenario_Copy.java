/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Findings_TestClasses;


import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Findings_PageObjects.Audit_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Findings Action Main Scenario copy",
        createNewBrowserInstance = false
)

public class FR2_Capture_Findings_Actions_MainScenario_Copy extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR2_Capture_Findings_Actions_MainScenario_Copy()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!NavigateToFindings()){
            return narrator.testFailed("Failed due - " + error);
        }
        if (!NavigateToSignOff()){
            return narrator.testFailed("Failed due - " + error);
        }
        if(!NavigateAuditActions()){
            return narrator.testFailed("Failed due - " + error);
        }
        if(!NavigateActionFeedback()){
            return narrator.testFailed("Failed due - " + error);
        }
        if(!validateComplete()){
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Completed navigate to Audit Actions");
    }

    public boolean NavigateToFindings(){
        //Findings tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Af_tab())){
            error = "Failed to wait for 'Findings' tab";
            return false;
        }
        pause(2000);
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Af_tab())){
            error = "Failed to click on 'Findings' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Findings' tab");
        
        //Submit SignOff button
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.audit_signoff())){
            error = "Failed to wait for 'Submit SignOff' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.audit_signoff())){
            error = "Failed to click on 'Submit SignOff' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Submit SignOff' button.");
        
        //Save
        if (SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Audit_PageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        
        return true;
    }    
    public boolean NavigateToSignOff(){
        //Navigate to SignOff tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.signOff_tab())){
            error = "Failed to wait for 'Sign Off' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.signOff_tab())){
            error = "Failed to click on 'Sign Off' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Sign Off' page.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.signOff_addButton())){
            error = "Failed to wait for 'Sign Off Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.signOff_addButton())){
            error = "Failed to click on 'Sign Off Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

         //Process flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.signOff_processflow())){
            error = "Failed to wait for 'process flow' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.signOff_processflow())){
            error = "Failed to click 'process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Process Flow' butoon.");
        
        //Auditor type //Senior auditor
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.auditorType_Tab())){
            error = "Failed to wait for 'Audit type' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.auditorType_Tab())){
            error = "Failed to click on 'Audit type' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.auditorType(getData("Auditor Type")))){
            error = "Failed to wait for '" + getData("Auditor Type") + "' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.auditorType(getData("Auditor Type")))){
            error = "Failed to click on '" + getData("Auditor Type") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Auditor Type : '" + getData("Auditor Type") + "'.");
        
        //Auditor
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.auditor_Tab())){
            error = "Failed to wait for 'Audit' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.auditor_Tab())){
            error = "Failed to click on 'Audit' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.auditor(getData("Auditor")))){
            error = "Failed to wait for '" + getData("Auditor") + "' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.auditor(getData("Auditor")))){
            error = "Failed to click on '" + getData("Auditor") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Auditor : '" + getData("Auditor") + "'.");
        
        //Sign Off 
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.signOff_Tab())){
            error = "Failed to wait for 'Sign Off' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.signOff_Tab())){
            error = "Failed to click on 'Sign Off' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.signOff(getData("Sign Off")))){
            error = "Failed to wait for '" + getData("Sign Off") + "' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.signOff(getData("Sign Off")))){
            error = "Failed to click on '" + getData("Sign Off") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Sign Off : '" + getData("Sign Off") + "'.");
        
        //Comments
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.comments())){
            error = "Failed to wait for 'Comments' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Audit_PageObjects.comments(), getData("Comments"))){
            error = "Failed to click on 'Comments' tab.";
            return false;
        }
        //Emailed to manager 
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.emailTo(getData("Email To")))){
            error = "Failed to wait for 'Email comments to:' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.emailTo(getData("Email To")))){
            error = "Failed to click on '" + getData("Email To") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Email comments to: '" + getData("Email To") + "' .");
        
        //Save
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.signOff_saveButton2())){
            error = "Failed to wait for Save button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.signOff_saveButton2())){
            error = "Failed to click Save button.";
            return false;
        }
        
        if (SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Audit_PageObjects.saveWait2(), 400)) {
                error = "Website took too long to load wait reached the time out";
                return false;
            }
        }
        pause(5000);
        
        //Close Audit/ Audit Sign Off
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.close_AuditSignOffTab())){
            error = "Failed to wait for 'X' button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.close_AuditSignOffTab())){
            error = "Failed to click on 'X' button";
            return false;
        }
        
        return true;
    }
public boolean NavigateAuditActions(){ 
    //Navigate to Audits Actions
    if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Actions_tab())){
        error = "Failed to wait for 'Audits Actions' tab.";
        return false;
    }
    if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Actions_tab())){
        error = "Failed to click on 'Audits Actions' tab.";
        return false;
    }
    narrator.stepPassedWithScreenShot("Successfully navigated to 'Audits Actions' tab.");

    //Add button
    if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Actions_add())){
        error = "Failed to wait for 'Add' button.";
        return false;
    }
    if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Actions_add())){
        error = "Failed to click on 'Add' button.";
        return false;
    }
    narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

    //Process Flow
    if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.findingsAudit_ProcessFlow())){
        error = "Failed to wait for 'Process Flow' button.";
        return false;
    }
    if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.findingsAudit_ProcessFlow())){
        error = "Failed to wait for 'Process Flow' button.";
        return false;
    }
    narrator.stepPassedWithScreenShot("Successfully click 'Process Flow' button.");

    //Description
    if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Actions_desc())){
        error = "Failed to wait for 'Description' textarea.";
        return false;
    }
    if(!SeleniumDriverInstance.enterTextByXpath(Audit_PageObjects.Audit_Actions_desc(), getData("Description"))){
        error = "Failed to enter '" + getData("Description") + "' into Description textarea.";
        return false;
    }
    narrator.stepPassedWithScreenShot("Description: '" + getData("Description") + "'.");

    //Department Responsible
    if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Actions_deptresp_dropdown())){
        error = "Failed to wait for Department Responsible dropdown.";
        return false;
    }
    if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Actions_deptresp_dropdown())){
        error = "Failed to click Department Responsible dropdown.";
        return false;
    }
    if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Actions_deptresp_select(getData("Department Responsible")))){
        error = "Failed to wait for '" + getData("Department Responsible") + "' in Department Responsible dropdown.";
        return false;
    }
    if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Actions_deptresp_select(getData("Department Responsible")))){
        error = "Failed to click '" + getData("Department Responsible") + "' from Department Responsible dropdown.";
        return false;
    }
    narrator.stepPassedWithScreenShot("Department Responsible: '" + getData("Department Responsible") + "'.");

    //Responsible Person
    if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Actions_respper_dropdown())){
        error = "Failed to wait for Responsible Person dropdown.";
        return false;
    }        
    if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Actions_respper_dropdown())){
        error = "Failed to click Responsible Person dropdown.";
        return false;
    }
    if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Actions_respper_select(getData("Responsible Person")))){
        error = "Failed to wait for '" + getData("Responsible Person") + "' in Responsible Person dropdown.";
        return false;
    }
    if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Actions_respper_select(getData("Responsible Person")))){
        error = "Failed to click '"+ getData("Responsible Person") + "' from Responsible Person dropdown.";
        return false;
    }
    narrator.stepPassedWithScreenShot("Responsible Person: '" + getData("Responsible Person") + "'.");

    //Due Date
    if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Actions_ddate())){
        error = "Failed to wait for Due Date textarea.";
        return false;
    }
    if(!SeleniumDriverInstance.enterTextByXpath(Audit_PageObjects.Audit_Actions_ddate(), endDate)){
        error = "Failed to enter '" + endDate + "' into due date textarea.";
        return false;
    }
    narrator.stepPassedWithScreenShot("Due date: '" + endDate + "'.");

    //Save
    if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Actions_save2())){
        error = "Failed to wait for Save button.";
        return false;
    }
    if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Actions_save2())){
        error = "Failed to click Save button.";
        return false;
    }

    //Save Mask
    if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Actions_savewait())){
        error = "Failed to wait for Save end.";
        return false;
    }

    return true;
}
public boolean NavigateActionFeedback(){
    //Navigate to Actions Feedback
    if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.actionsFeedbackTab())){
        error = "Failed to wait for 'Action Feedback' tab.";
        return false;
    }
    if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.actionsFeedbackTab())){
        error = "Failed to click on 'Action Feedback' tab.";
        return false;
    }
    narrator.stepPassedWithScreenShot("Successfully navigated to 'Action Feedback' tab.");

    //Action Feedback Add Button
    if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.addButton())){
        error = "Failed to wait for 'Feedback Add' button.";
        return false;
    }
    if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.addButton())){
        error = "Failed to click on 'Feedback Add' button.";
        return false;
    }
    narrator.stepPassedWithScreenShot("Successfully clicked 'Feedback Add' button.");

    //Process Flow
    if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.actionFeedback_ProcessFlow())){
        error = "Failed to wait for 'Process Flow' button";
        return false;
    }
    if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.actionFeedback_ProcessFlow())){
        error = "Failed to click on 'Process Flow' button";
        return false;
    }
    narrator.stepPassedWithScreenShot("Successfully clicked 'Process Flow' button.");

    //Action Feedback
    if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.actionFeedback())){
        error = "Failed to wait for 'Action Feedback' textarea";
        return false;
    }
    if(!SeleniumDriverInstance.enterTextByXpath(Audit_PageObjects.actionFeedback(), getData("Action Feedback"))){
        error = "Failed to enter '" + getData("Action Feedback") + "' into 'Action Feedback' textarea";
        return false;
    }
    narrator.stepPassedWithScreenShot("Action Feedback: '" + getData("Action Feedback") + "'.");

    //Auto Complete
    if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.actionCompleteTab())){
        error = "Failed to wait for 'Action Complete' dropdown.";
        return false;
    }
    if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.actionCompleteTab())){
        error = "Failed to click on 'Action Complete' dropdown.";
        return false;
    }
    if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.select_actionComplete(getData("Action Complete")))){
        error = "Failed to wait for 'Action Complete' dropdown.";
        return false;
    }
    if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.select_actionComplete(getData("Action Complete")))){
        error = "Failed to click '" + getData("Action Complete") + "' on 'Action Complete' dropdown.";
        return false;
    }
    narrator.stepPassedWithScreenShot("Action Complete: '" + getData("Action Complete") + "'.");


    //Send action feedback to
    if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.feedbackTo(getData("Feedback To")))){
        error = "Failed to wait for 'Send action feedback to' dropdown.";
        return false;
    }
    if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.feedbackTo(getData("Feedback To")))){
        error = "Failed to click on 'Send action feedback to' dropdown.";
        return false;
    }
    narrator.stepPassedWithScreenShot("Send action feedback to: '" + getData("Feedback To") + "'.");

    //Save
    if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.actionFeedback_saveButton())){
        error = "Failed to wait for 'Feedback Save' button.";
        return false;
    }
    if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.actionFeedback_saveButton())){
        error = "Failed to click 'Feedback Save' button.";
        return false;
    }
    SeleniumDriverInstance.pause(5000);
    if (SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.saveWait(), 2)) {
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Audit_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out.";
            return false;
        }
    }
    narrator.stepPassedWithScreenShot("Successfully clicked 'Feedback Save' button.");
    SeleniumDriverInstance.pause(5000);

    //Close Action Feedback 
    if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.close_AuditFeedbackTab())){
        error = "Failed to wait for 'X' button.";
        return false;
    }
    if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.close_AuditFeedbackTab())){
        error = "Failed to click on 'X' button.";
        return false;
    }
    pause(2000);
    //Process flow
    if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.actions_ProcessFlow())){
        error = "Failed to wait for 'process flow' button.";
        return false;
    }
    if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.actions_ProcessFlow())){
        error = "Failed to click 'process flow' button.";
        return false;
    }
    narrator.stepPassedWithScreenShot("Successfully clicked 'Process Flow' butoon.");

    return true;
}

public boolean validateComplete(){
    //Close Action Feedback 
    if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.close_AuditActionTab())){
        error = "Failed to wait for 'X' button.";
        return false;
    }
    if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.close_AuditActionTab())){
        error = "Failed to click on 'X' button.";
        return false;
    }

    pause(2000);
    //Process flow
    if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_processflow())){
        error = "Failed to wait for 'process flow' button.";
        return false;
    }
    if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_processflow())){
        error = "Failed to click 'process flow' button.";
        return false;
    }
    narrator.stepPassedWithScreenShot("Successfully clicked 'Process Flow' butoon.");

    if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.validate_status())){
        error = "Failed to wait for 'Status' dropdown.";
        return false;
    }
    if(!SeleniumDriverInstance.ValidateByText(Audit_PageObjects.validate_status(), getData("Status"))){
        error = "Failed to validate 'Status'.";
        return false;
    }
    narrator.stepPassedWithScreenShot("Successfully validated 'Status': '" + getData("Status") + "'.");

    if(getData("Close Browser").equalsIgnoreCase("TRUE")){
        SeleniumDriverInstance.Driver.close();
    }

    return true;
}
    
    
}
