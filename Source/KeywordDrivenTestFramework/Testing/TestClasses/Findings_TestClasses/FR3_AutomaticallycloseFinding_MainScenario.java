/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Findings_TestClasses;


import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Findings_PageObjects.Audit_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Findings_PageObjects.Findings_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Automatically close finding - Main Scenario",
        createNewBrowserInstance = false
)

public class FR3_AutomaticallycloseFinding_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR3_AutomaticallycloseFinding_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!NavigateToFindings()){
            return narrator.testFailed("Failed due - " + error);
        }
        if (!AddAuditFindingsActions()){
            return narrator.testFailed("Failed due - " + error);
        }
        if (!NavigateActionFeedback()){
            return narrator.testFailed("Failed due - " + error);
        }
        if(!AddActionSignOff()){
            return narrator.testFailed("Failed due - " + error);
        }
        if(!validateComplete()){
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Automatically ");
    }

    public boolean NavigateToFindings(){
        FR1_Capture_Findings_MainScenario_Part3 FR1_Part3 =  new FR1_Capture_Findings_MainScenario_Part3();
        FR1_Part3.navigateToAudit();
        
        //Findings tab
         pause(10000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Af_tab())){
            error = "Failed to wait for 'Findings' tab";
            return false;
        }
       
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Af_tab())){
            error = "Failed to click on 'Findings' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Findings' tab");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.af_add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.af_add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
        
        FR1_Capture_Findings_MainScenario_Part5 FR5 = new FR1_Capture_Findings_MainScenario_Part5();
        FR5.enterDetails();
        
        //Root cause
        if(!SeleniumDriverInstance.waitForElementByXpath(Findings_PageObjects.rootCause_Tab())){
            error = "Failed to wait for 'Root Cause' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Findings_PageObjects.rootCause_Tab())){
            error = "Failed to click on 'Root Cause' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Findings_PageObjects.rootCause_Select(getData("Root cause")))){
            error = "Failed to wait for '" + getData("Root cause") + "' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Findings_PageObjects.rootCause_Select(getData("Root cause")))){
            error = "Failed to click on '" + getData("Root cause") + "' option.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.rootCause_arrow())){
            error = "Failed to wait for Root cause arrow.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.rootCause_arrow())){
            error = "Failed to click Root cause arrow.";
            return false;
        }
        
        //Save to continue
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.saveFindingsButton())){
            error = "Failed to wait for 'Save to continue' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.saveFindingsButton())){
            error = "Failed to click 'Save to continue' button.";
            return false;
        }
//        //Save mask
//        if (SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.saveWait(), 2)) {
//            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Audit_PageObjects.saveWait2(), 400)) {
//                error = "Webside too long to load wait reached the time out";
//                return false;
//            }
//        }
        pause(25000);
        return true;
    }
    public boolean AddAuditFindingsActions(){ 
        //Navigate to Audits Actions
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.AuditFindingsActions_add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.AuditFindingsActions_add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
        
        //Process Flow
        pause(10000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.auditFindingsAudit_ProcessFlow())){
            error = "Failed to wait for 'Process Flow' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.auditFindingsAudit_ProcessFlow())){
            error = "Failed to click on 'Process Flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process Flow' button.");
        
        //Description
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Actions_desc())){
            error = "Failed to wait for 'Description' textarea.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Audit_PageObjects.Audit_Actions_desc(), getData("Description"))){
            error = "Failed to enter '" + getData("Description") + "' into Description textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Description: '" + getData("Description") + "'.");
        
        //Department Responsible
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Actions_deptresp_dropdown())){
            error = "Failed to wait for Department Responsible dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Actions_deptresp_dropdown())){
            error = "Failed to click Department Responsible dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Actions_deptresp_select(getData("Department Responsible")))){
            error = "Failed to wait for '" + getData("Department Responsible") + "' in Department Responsible dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Actions_deptresp_select(getData("Department Responsible")))){
            error = "Failed to click '" + getData("Department Responsible") + "' from Department Responsible dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Department Responsible: '" + getData("Department Responsible") + "'.");
        
        //Responsible Person
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Actions_respper_dropdown())){
            error = "Failed to wait for Responsible Person dropdown.";
            return false;
        }        
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Actions_respper_dropdown())){
            error = "Failed to click Responsible Person dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Actions_respper_select(getData("Responsible Person")))){
            error = "Failed to wait for '" + getData("Responsible Person") + "' in Responsible Person dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Actions_respper_select(getData("Responsible Person")))){
            error = "Failed to click '"+ getData("Responsible Person") + "' from Responsible Person dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Responsible Person: '" + getData("Responsible Person") + "'.");
        
        //Due Date
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Actions_ddate())){
            error = "Failed to wait for Due Date textarea.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Audit_PageObjects.Audit_Actions_ddate(), endDate)){
            error = "Failed to enter '" + endDate + "' into due date textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Due date: '" + endDate + "'.");
        
        //Save
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Actions_save())){
            error = "Failed to wait for Save button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Actions_save())){
            error = "Failed to click Save button.";
            return false;
        }
        
        pause(20000);
//        //Save Mask
//        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Actions_savewait())){
//            error = "Failed to wait for Save end.";
//            return false;
//        }
        
        return true;
    }
    public boolean NavigateActionFeedback(){
        //Navigate to Actions Feedback
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.actionsFeedbackTab())){
            error = "Failed to wait for 'Action Feedback' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.actionsFeedbackTab())){
            error = "Failed to click on 'Action Feedback' tab.";
            return false;
        }
        
        pause(5000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Action Feedback' tab.");
        
        //Action Feedback Add Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.addButton())){
            error = "Failed to wait for 'Feedback Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.addButton())){
            error = "Failed to click on 'Feedback Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Feedback Add' button.");
        
        pause(10000);
        //Process Flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.actionFeedback_ProcessFlow())){
            error = "Failed to wait for 'Process Flow' button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.actionFeedback_ProcessFlow())){
            error = "Failed to click on 'Process Flow' button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Process Flow' button.");
        
        //Action Feedback
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.actionFeedback())){
            error = "Failed to wait for 'Action Feedback' textarea";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Audit_PageObjects.actionFeedback(), getData("Action Feedback"))){
            error = "Failed to enter '" + getData("Action Feedback") + "' into 'Action Feedback' textarea";
            return false;
        }
        narrator.stepPassedWithScreenShot("Action Feedback: '" + getData("Action Feedback") + "'.");
        
        //Auto Complete
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.actionCompleteTab())){
            error = "Failed to wait for 'Action Complete' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.actionCompleteTab())){
            error = "Failed to click on 'Action Complete' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.select_actionComplete2(getData("Action Complete")))){
            error = "Failed to wait for 'Action Complete' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.select_actionComplete2(getData("Action Complete")))){
            error = "Failed to click '" + getData("Action Complete") + "' on 'Action Complete' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Action Complete: '" + getData("Action Complete") + "'.");
        
        
        //Send action feedback to
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.feedbackTo(getData("Feedback To")))){
            error = "Failed to wait for 'Send action feedback to' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.feedbackTo(getData("Feedback To")))){
            error = "Failed to click on 'Send action feedback to' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Send action feedback to: '" + getData("Feedback To") + "'.");
        
        //Save
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.auditActionFeedback_saveButton())){
            error = "Failed to wait for 'Feedback Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.auditActionFeedback_saveButton())){
            error = "Failed to click 'Feedback Save' button.";
            return false;
        }
        SeleniumDriverInstance.pause(5000);
        if (SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Audit_PageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out.";
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Feedback Save' button.");
        SeleniumDriverInstance.pause(15000);
        
        //Close Action Feedback 
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.auditActionsFindings_Close(), 10000)){
            error = "Failed to wait for 'X' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.auditActionsFindings_Close())){
            error = "Failed to click on 'X' button.";
            return false;
        }
        pause(15000);
        //Process Flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.auditFindingsAudit_ProcessFlow(), 2000)){
            error = "Failed to wait for 'Process Flow' button.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.auditFindingsAudit_ProcessFlow())){
            error = "Failed to wait for 'Process Flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process Flow' button.");
        
        return true;
    }
    public boolean AddActionSignOff(){
        //Navigate to Sign Off tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Findings_PageObjects.signOff_tab())){
            error = "Failed to wait for 'Sign Off' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Findings_PageObjects.signOff_tab())){
            error = "Failed to click on 'Sign Off' tab.";
            return false;
        }
        pause(5000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Sign Off' tab.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Findings_PageObjects.signOff_addButton())){
            error = "Failed to wait for 'Sign Off Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Findings_PageObjects.signOff_addButton())){
            error = "Failed to click on 'Sign Off Add' button.";
            return false;
        }
        pause(10000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
        
        //Process flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Findings_PageObjects.actionSignOff_ProcessFlow())){
            error = "Failed to wait for 'Action Sign Off' Process Flow.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Findings_PageObjects.actionSignOff_ProcessFlow())){
            error = "Failed to click on 'Action Sign Off' Process Flow.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Process Flow' button.");
        
        //Sign Off Action
        if(!SeleniumDriverInstance.waitForElementByXpath(Findings_PageObjects.signOffAction_Tab())){
            error = "Failed to wait for 'Sign off action' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Findings_PageObjects.signOffAction_Tab())){
            error = "Failed to click on 'Sign off action' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Findings_PageObjects.signOffAction_YesOption(getData("Sign Off")))){
            error = "Failed to wait for 'Sign off action' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Findings_PageObjects.signOffAction_YesOption(getData("Sign Off")))){
            error = "Failed to click on 'Sign off action' option.";
            return false;
        }
        
        //Comments
        if(!SeleniumDriverInstance.waitForElementByXpath(Findings_PageObjects.comments())){
            error = "Failed to wait for 'Comments' textarea.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Findings_PageObjects.comments(), getData("Comments"))){
            error = "Failed to enter '" + getData("Comments") + "' into 'Comments' textarea.";
            return false;
        }
        
        if(getData("Additonal Users").equalsIgnoreCase("TRUE")){
            //Send feedback to additional users
            if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.aditionalUsers(getData("Additional Users Feedback")))){
                error = "Failed to wait for '" + getData("Additional Users Feedback") + "' option.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.aditionalUsers(getData("Additional Users Feedback")))){
                error = "Failed to click on '" + getData("Additional Users Feedback") + "' option.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Additional Users Feedback: '" + getData("Additional Users Feedback") + "' .");
        }
        //Save
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.ActionSignOff_saveButton())){
            error = "Failed to wait for Save button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.ActionSignOff_saveButton())){
            error = "Failed to click Save button.";
            return false;
        }
        
        if (SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Audit_PageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        pause(10000);
        //Close Acion Feedback 
        if(!SeleniumDriverInstance.waitForElementByXpath(Findings_PageObjects.actionSignOff_Close(), 5000)){
            error = "Failed to wait for 'X' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Findings_PageObjects.actionSignOff_Close())){
            error = "Failed to click on 'X' button.";
            return false;
        }
        pause(5000);
        //Process Flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Findings_PageObjects.AuditFindingsAction_processFlow(), 5000)){
            error = "Failed to wait for 'Process Flow' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Findings_PageObjects.AuditFindingsAction_processFlow())){
            error = "Failed to click on 'Process Flow' button.";
            return false;
        }
        
        return true;
    }
    public boolean validateComplete(){
        pause(10000);
        //Close Action Feedback 
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.close_AuditFindingsActionTab(), 10000)){
            error = "Failed to wait for 'X' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.close_AuditFindingsActionTab())){
            error = "Failed to click on 'X' button.";
            return false;
        }
        
        pause(5000);
        //Process flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.fa_processflow(), 2000)){
            error = "Failed to wait for 'process flow' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.fa_processflow())){
            error = "Failed to click 'process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Process Flow' butoon.");
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.findingStatus())){
            error = "Failed to wait for 'Status' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.ValidateByText(Audit_PageObjects.findingStatus(), getData("Status"))){
            error = "Failed to validate 'Status'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully validated 'Status': '" + getData("Status") + "'.");
        
        if(getData("Close Browser").equalsIgnoreCase("TRUE")){
            SeleniumDriverInstance.Driver.close();
        }
        
        return true;
    }
 
}
