/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Findings_TestClasses;


import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Findings_PageObjects.Audit_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Findings_PageObjects.Findings_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Findings Action Main Scenario",
        createNewBrowserInstance = false
)

public class FR2_Capture_Findings_Actions_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR2_Capture_Findings_Actions_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!IsometrixNavigateAuditActions())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        
        if(!enterDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Completed navigate to Audit Actions");
    }

    public boolean IsometrixNavigateAuditActions(){  
        //Process Flow
//        pause(2000);
//        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.fa_processflow())){
//            error = "Failed to wait for 'Process Flow' button.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.fa_processflow())){
//            error = "Failed to click on 'Process Flow' button.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow.");
        pause(5000);
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.fa_add(), 10000)){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.fa_add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }
    
    //Enter data
    public boolean enterDetails(){
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Actions_desc())){
            error = "Failed to wait for 'Description' textarea.";
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextByXpath(Audit_PageObjects.Audit_Actions_desc(), testData.getData("Description"))){
            error = "Failed to enter '"+testData.getData("Description")+"' into Description textarea.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Actions_deptresp_dropdown())){
            error = "Failed to wait for Department Responsible dropdown.";
            return false;
        }
        pause(1000);
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Actions_deptresp_dropdown())){
            error = "Failed to click Department Responsible dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Actions_deptresp_select(testData.getData("Department Responsible")))){
            error = "Failed to wait for '"+testData.getData("Department Responsible")+"' in Department Responsible dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Actions_deptresp_select(testData.getData("Department Responsible")))){
            error = "Failed to click '"+testData.getData("Department Responsible")+"' from Department Responsible dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Actions_respper_dropdown())){
            error = "Failed to wait for Responsible Person dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Actions_respper_dropdown())){
            error = "Failed to click Responsible Person dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Actions_respper_select(testData.getData("Responsible Person")))){
            error = "Failed to wait for '"+testData.getData("Responsible Person")+"' in Responsible Person dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Actions_respper_select(testData.getData("Responsible Person")))){
            error = "Failed to click '"+testData.getData("Responsible Person")+"' from Responsible Person dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Actions_ddate())){
            error = "Failed to wait for Due Date textarea.";
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextByXpath(Audit_PageObjects.Audit_Actions_ddate(), endDate)){
            error = "Failed to enter '"+endDate+"' into due date textarea.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Actions_save())){
            error = "Failed to wait for Save button.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Actions_save())){
            error = "Failed to click Save button.";
            return false;
        }
        
        pause(15000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Actions_savewait())){
            error = "Failed to wait for Save end.";
            return false;
        }
        
//        //Check if the record has been Saved
//        if(!SeleniumDriverInstance.waitForElementByXpath(Findings_PageObjects.inspection_RecordSaved_popup())){
//            error = "Failed to wait for 'Record Saved' popup.";
//            return false;
//        }
//        
//        String saved = SeleniumDriverInstance.retrieveTextByXpath(Findings_PageObjects.inspection_RecordSaved_popup());
//        
//        if(saved.equals("Record saved")){
//            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
//        }else{   
//            if(!SeleniumDriverInstance.waitForElementByXpath(Findings_PageObjects.failed())){
//                error = "Failed to wait for error message.";
//                return false;
//            }
//
//            String failed = SeleniumDriverInstance.retrieveTextByXpath(Findings_PageObjects.failed());
//
//            if(failed.equals("ERROR: Record could not be saved")){
//                error = "Failed to save record.";
//                return false;
//            }
//        }
        
        return true;
    }

}
