/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.sugarcrm.pages;

import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Testing.PageObjects.SugarCRM_PageObject;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Utilities.SeleniumDriverUtility;

/**
 *
 * @author amarais
 */
public class SugarCRMPages
{
    private SeleniumDriverUtility SeleniumDriverInstance = new SeleniumDriverUtility(Enums.BrowserType.Chrome);
    private Narrator narrator = new Narrator();
    
    public String logintoSugarCRM(String username, String password){
        SeleniumDriverInstance.startDriver();
        //Navigate to the URL
        if (!SeleniumDriverInstance.navigateTo(SugarCRM_PageObject.SugarURL())) {
            return "Failed to navigate to SugarCRM.";
        }
      // narrator.stepPassedWithScreenShot("Successfully navigated through Sugar_CRM");
       SeleniumDriverInstance.takeScreenShot("11Successfully navigated through Sugar_CRM", false);
       
        //Fill in the field
        if (!SeleniumDriverInstance.enterTextByXpath(SugarCRM_PageObject.username(), username)) {
            return "Failed to enter text '" + username + "' into username field.";
        }
        if (!SeleniumDriverInstance.enterTextByXpath(SugarCRM_PageObject.password(), password)) {
           return "Failed to enter text '" + password + "' into password field.";
        }
        narrator.stepPassedWithScreenShot("username '" + username + "' Password '" + password + "' Successfully loaded");
        
        //Click the Login button
        if (!SeleniumDriverInstance.clickElementbyXpath(SugarCRM_PageObject.loginButton())) {
            return "Failed to click 'Login' button.";
        }
        
        return null;
    }
    
    public String validateHomeScreen(String firstname, String lastname) {
        if(!SeleniumDriverInstance.waitForElementPresentByXpath(SugarCRM_PageObject.HomePage())){
            return "Failed to validate home page text 'Welcome'";
        }
        //narrator.stepPassedWithScreenShot("Successfully logged in by " + firstname +" "+ lastname);
        SeleniumDriverInstance.takeScreenShot("Successfully logged in by " + firstname +" "+ lastname, true);
        
        return null;
    }

    public String navigateToContacts(){
        //Click the button Sales And Contacts
        if (!SeleniumDriverInstance.clickElementbyXpath(SugarCRM_PageObject.salesButton())) {
            return "Failed to click 'Sales Tab'";
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(SugarCRM_PageObject.contactsButton())) {
            return "Failed to click 'Contacts'";
        }
        //narrator.stepPassedWithScreenShot("Successfully navigated to Contacts");
        SeleniumDriverInstance.takeScreenShot("Successfully navigated to Contacts", true);
        
        return null;
    }

    public String createContact(String firstname, String lastname, String email) {
        //Clicks the Create 
        if(!SeleniumDriverInstance.clickElementbyXpath(SugarCRM_PageObject.createButton())){
            return "Failed to click 'Create Link' button";
        }
        
        // Fill In the fields
        if (!SeleniumDriverInstance.enterTextByXpath(SugarCRM_PageObject.txtFirstName(), firstname)) {
            return "Failed to enter text '" + firstname + "' into first name field.";
        }
        if (!SeleniumDriverInstance.enterTextByXpath(SugarCRM_PageObject.txtLastName(), lastname)) {
            return "Failed to enter text '" + lastname + "' into last name field.";
        }
        if (!SeleniumDriverInstance.enterTextByXpath(SugarCRM_PageObject.txtEmail(), email)) {
            return "Failed to enter text '" + email + "' into email field.";
        }
        //narrator.stepPassedWithScreenShot("Successfully entered text '" + firstname + "' into first name, '" + lastname + "' into last name and '" + email + "' into email field. ");
        SeleniumDriverInstance.takeScreenShot("Successfully entered text '" + firstname + "' into first name, '" + lastname + "' into last name and '" + email + "' into email field. ", true);
        
        //Save Button
        if (!SeleniumDriverInstance.clickElementbyXpath(SugarCRM_PageObject.saveButton())) {
            return "Failed to click 'Save' button.";
        }
        
        return null;
    }

    public String validateContact(String firstname, String lastname) {
        String fullname = firstname + " " + lastname;
        if(!SeleniumDriverInstance.ValidateByText(SugarCRM_PageObject.vFullName(), fullname) ){
            return "Failed to validate the contact " + fullname;
        }
        narrator.stepPassedWithScreenShot("Contact '" + fullname + "' has successfully created.");

        return null;
    }
      
    public String deleteContact(String firstname) {
        //Delete Contact
        if(!SeleniumDriverInstance.clickElementbyXpath(SugarCRM_PageObject.selectCheckBox(firstname))){
            return "Failed to click the check box" + firstname;
        }
        narrator.stepPassedWithScreenShot("Contact: " + firstname + " has been checked");
        
        if(!SeleniumDriverInstance.clickElementbyXpath(SugarCRM_PageObject.deleteContact())){
            return "Failed to click 'Delete' button for contact";
        }
        if(!SeleniumDriverInstance.alertHandler()){
            return "Failed to click Delete Confirmation";
        }
        narrator.stepPassedWithScreenShot("Contact: " + firstname + " has been deleted");
        
        return null;
    }
    
    public String logoutOfSugarCRM() {
        //Click logout 
        if (!SeleniumDriverInstance.clickElementbyXpath(SugarCRM_PageObject.logOutButton())) {
            return "Failed to click 'Log Out' button.";
        }
        narrator.stepPassedWithScreenShot("Successfully clicked logged out link");
        
        return null;        
    }
    
    public String validateLogout() {
        //Validating the the logout of Sugar_CRM
        if(!SeleniumDriverInstance.waitForElementPresentByXpath(SugarCRM_PageObject.loginButton())){
            return "Failed to logout";
        }
        narrator.stepPassedWithScreenShot("Successfully logged out of Sugar_CRM");
        
        //Closing the drive instance
        SeleniumDriverInstance.shutDown();
        SeleniumDriverInstance.Driver.close();
        return null;
    }
        

}
