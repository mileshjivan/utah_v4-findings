/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Findings_PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author syotsi
 */
public class Findings_PageObjects extends BaseClass {
    
    public static String MaskBlock() {
        return"//div[@class='ui inverted dimmer active']";
    }

    public static String MaskNone() {
        return"//div[@class='ui inverted dimmer']";
    }    
    
     public static String saveWait() {
        return "//div[@class='ui inverted dimmer active']";
    }
    public static String saveWait2() {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }
    //Navigate to Suggestions and Innovations
    public static String navigate_EHS() {
        return "//label[contains(text(),'Environmental, Health & Safety')]";
    }
    
    public static String CFM_tab(){
        return "//label[text()='Central Findings Manager']";
    }
    
    public static String CFM_RootCause_dropdown(){
        return "//div[@id='control_E4B7948E-2728-48B0-9981-05057C548333']";
    }
    
    public static String CFM_RootCause_select(String text){
        return "(//a[text()='"+text+"']/i)[1]";
    }
    
    public static String CFM_RootCause_arrow(){
        return "//div[@id='control_E4B7948E-2728-48B0-9981-05057C548333']//b[@class='select3-down drop_click']";
    }
    
    public static String CFM_RCC_textarea(){
        return "//div[@id='control_B0C4CB46-6EF6-43B0-B739-6AD8110C1A1A']/div/div/textarea";
    }
    
    public static String Save(){
        return "(//div[text()='Save'])[4]";
    }
    
    public static String supporting_tab(){
        return "//div[@id='tab_E3A1B1CF-5B69-453A-9FDB-824EBD012F54']";
    }
    
    public static String supporting_tab_3(){
        return "(//div[text()='Supporting Documents'])[3]";
    }
    
     public static String linkADoc_buttonxpath() {
        return "//div[@id='control_26D8DE84-DA5D-40D8-A3C5-890D11364B66']//b[@original-title='Link to a document']";
    }

    public static String urlInput_TextAreaxpath() {
        return "//input[@id='urlValue']";
    }

    public static String tile_TextAreaxpath() {
        return "//input[@id='urlTitle']";
    }

    public static String linkADoc_Add_buttonxpath() {
        return "//div[@id='btnConfirmYes']//div[text()='Add']";
    }
    
    public static String processFlow(){
        return "//div[@id='btnProcessFlow_form_C9BF4B41-F36D-48FC-8F3F-FE6BC39DEE07']";
    }

    public static String close_X() {
        return "(//i[@class='close icon cross'])[11]";
    }
    public static String actionSignOff_Close(){
        return "(//div[@id='form_E243A701-48B0-4D32-9EE8-EB3FE5B9D019']//i[@class='close icon cross'])[1]";
    }

    public static String auditActionRecord() {
        return "(//div[@id='control_A14067F4-B0D1-468C-B5B8-D0B7BA136E70']//table)[3]";
    }

    public static String actionFeedbackTab() {
        return "//li[@id='tab_99358277-8A7C-40A1-9ED3-7613E44800C2']";
    }

    public static String actionFeedbackAddButton() {
        return "(//div[@id='control_2FD21E6C-8939-4BE7-B454-7B622DA3C71B']//div[contains(text(),'Add')])[1]";
    }

    public static String actionFeedback_Textarea() {
        return "//div[@id='control_B9A4BEF3-B4D1-491D-881A-6021DDE86169']//textarea";
    }

    public static String actionCompleted_Dropdown() {
        return "//div[@id='control_40EFC614-1BC1-4F14-934E-928BF0980FF2']";
    }

    public static String actionCompleted_Option(String data) {
        return "(//div[@id='control_25A2CAD4-9F0A-4E67-B62A-6A4EF1C9A920']/../../../../../../../..//a[text()='" + data + "'])[1]";
    }

    public static String sendActionTo_Option(String data) {
        return "(//div[@id='control_4854CDB6-7DB2-48B6-B92F-7F2E008CD5E2']//a[contains(text(),'" + data + "')]/i)[1]";
    }

    public static String actionFeedback_ProcessFlow() {
        return "//div[@id='btnProcessFlow_form_70F855C4-38C3-4717-ADA2-27C0F8880E46']";
    }

    public static String close_X4() {
        return "(//i[@class='close icon cross'])[4]";
    }

    public static String close_X13() {
        return "(//i[@class='close icon cross'])[13]";
    }
    
    public static String signOff_tab() {
        return "//li[@id='tab_9AC05ECF-DEDC-44FE-A265-F2D1600C2EC9']//div[contains(text(),'Sign Off')]";
    }

    public static String signOff_addButton() {
        return "//div[@id='control_76DA08C4-FD33-4D3D-B678-8CBD39CE669E']//div[contains(text(),'Add')]";
    }

    public static String signOffAction_Tab() {
        return "//div[@id='control_7EB577FB-3A6B-4CAF-B5E5-1D9E6D8040F0']";
    }

    public static String signOffAction_YesOption(String data) {
        return "(//a[contains(text(),'" + data + "')])[2]";
    }
    public static String signOffAction_NoOption(String data) {
        return "(//a[contains(text(),'" + data + "')])[6]";
    }

    public static String comments() {
        return "//div[@id='control_55A6635D-1D32-4DB9-953C-3329A76E4F05']//textarea";
    }

    public static String actionSignOff_ProcessFlow() {
        return "//div[@id='btnProcessFlow_form_E243A701-48B0-4D32-9EE8-EB3FE5B9D019']";
    }

    public static String rootCause_Tab() {
        return "//div[@id='control_E4B7948E-2728-48B0-9981-05057C548333']";
    }

    public static String rootCause_Select(String data) {
        return "(//a[contains(text(),'" + data + "')]/i)[1]";
    }

    public static String AuditFindingsAction_processFlow() {
        return "//div[@id='btnProcessFlow_form_493EF828-C4E6-4204-9AEC-3ADF6E0501C0']";
    }
    public static String inspection_RecordSaved_popup() {
        return "//div[@id='divForms']//div[contains(text(),'Record saved')]";
    }
    public static String recordSaved_popup() {
        return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])[2]";
    }
    public static String failed(){
       return "(//div[@id='txtAlert'])[2]";
   }
    
}
